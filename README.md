# MSI19Z-scrabble
This project is part of the MSI course. Our goal is to create AI scrabble player.

# References
1. [Multi-player alpha-beta pruning (1989)](https://drive.google.com/open?id=1X3dltjgdte0UYn2_FDtRbS4jJ3BT5kWd) - from dr Okulicka - shallow pruning in perfect-information, n-player games
2. [The World's Fastest Scrabble Program (1988)](https://www.cs.cmu.edu/afs/cs/academic/class/15451-s06/www/lectures/scrabble.pdf) - lexicon representation, eager alg, pseudo-code
3. [A Faster Scrabble Move Generator Algorithm (1994)](https://ericsink.com/downloads/faster-scrabble-gordon.pdf) - lexicon representation, GADDAG
4. [Towards Perfect Play of Scrabble](https://cris.maastrichtuniversity.nl/portal/files/1504714/guid-b2c8ed9d-4f1f-4eb2-bf0f-7ad95196f373-ASSET1.0.pdf) - PhD thesis by Brian Sheppard (author of Maven). Long
5. [A Scrabble Artificial Intelligence Game (2017)](https://scholarworks.sjsu.edu/cgi/viewcontent.cgi?article=1574&context=etd_projects) - zacząć od opisu Quackle